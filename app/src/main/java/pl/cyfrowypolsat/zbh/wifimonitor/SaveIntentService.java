package pl.cyfrowypolsat.zbh.wifimonitor;

import android.annotation.TargetApi;
import android.app.IntentService;
import android.content.Intent;
import android.content.Context;
import android.os.Build;
import android.os.Environment;
import android.text.format.Time;
import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.util.Date;
import java.util.Formatter;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p/>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class SaveIntentService extends IntentService {
    // TODO: Rename actions, choose action names that describe tasks that this
    // IntentService can perform, e.g. ACTION_FETCH_NEW_ITEMS
    public static final String ACTION_GETLOG = "pl.cyfrowypolsat.zbh.wifimonitor.extra.GETLOG";

    public static final String EXTRA_TEXT = "pl.cyfrowypolsat.zbh.wifimonitor.extra.PARAM1";

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public SaveIntentService() {
        super("SaveIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_GETLOG.equals(action)) {
                handleActionGETLOG(intent.getStringExtra(EXTRA_TEXT));
            }
        }
    }

    private void handleActionGETLOG(String text) {
        appendLog(this, text);
    }

    private void appendLog(Context context, String text) {
        Time now = new Time();
        now.setToNow();
        CharSequence now_formated = new Formatter().format(
                "%02d:%02d:%02d", now.hour, now.minute, now.second).toString();
        Date date = new Date();
        DateFormat dateFormat = android.text.format.DateFormat.getDateFormat(context);
        File logFile = new File(Environment.getExternalStorageDirectory() + "/cpwifimonitorlog");
        Log.i("VoWiFi", logFile.getAbsolutePath());
        if (!logFile.exists())
        {
            try
            {
                logFile.createNewFile();
            }
            catch (IOException e)
            {
                Log.e("VoWiFi", "Cannot create a file!");
            }
        }
        try
        {
            //BufferedWriter for performance, true to set append to file flag
            BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true));
            buf.append("["+dateFormat.format(date)+"]["+now_formated+"]\t"+text);
            buf.newLine();
            buf.close();
        }
        catch (IOException e)
        {
            Log.e("VoWiFi", "IO Exception!");
        }
        try {
            BufferedReader reader = new BufferedReader(new FileReader(logFile));
            Log.i("VoWiFi", "READ: " + reader.readLine());
            reader.close();
        }
        catch (IOException e)
        {
            Log.e("VoWiFi", "Sth wen wrong!");
        }
        //getLogs((String) now_formated);
    }

//    private void getLogs(String now_formated) {
//        try {
//            // grab logcat
//            Process process = Runtime.getRuntime().exec("logcat -d");
//            BufferedReader bufferedReader = new BufferedReader(
//                    new InputStreamReader(process.getInputStream()));
//
//            StringBuilder log=new StringBuilder();
//            String line;
//            while ((line = bufferedReader.readLine()) != null) {
//                //if (line.matches("com\\.polkomtel*")) {
//                    log.append(line);
//                //}
//            }
//            File logFile = new File(Environment.getExternalStorageDirectory() + new Formatter().format(
//                    "/logcat.txt", now_formated
//            ).toString());
//            if (!logFile.exists()) {
//                logFile.createNewFile();
//            }
//            if (!log.equals("")) {
//                //BufferedWriter for performance, true to set append to file flag
//                BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true));
//                buf.append(log.toString());
//                buf.newLine();
//                buf.close();
//            }
//            // clear logcat
//            Runtime.getRuntime().exec("logcat -c");
//        } catch (IOException e) {
//        }
//    }
}
