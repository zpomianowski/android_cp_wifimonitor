package pl.cyfrowypolsat.zbh.wifimonitor;

import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Button buttonLoadLog = (Button) findViewById(R.id.buttonLoadLog);
        final Button buttonClearLog = (Button) findViewById(R.id.clearButton);
        buttonLoadLog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadLog();
            }
        });
        buttonClearLog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearLog();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public static String convertStreamToString(InputStream is) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        while ((line = reader.readLine()) != null) {
            sb.append(line).append("\n");
        }
        reader.close();
        return sb.toString();
    }

    private void loadLog() {
        File logFile = new File(Environment.getExternalStorageDirectory() + "/cpwifimonitorlog");
        final TextView logView = (TextView) findViewById(R.id.textLogView);
        try {
            if (!logFile.exists()) {
                logView.setText("");
                return;
            }
            FileInputStream fin = new FileInputStream(logFile);
            String ret = convertStreamToString(fin);
            fin.close();
            logView.setMovementMethod(new ScrollingMovementMethod());
            logView.setText(ret);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void clearLog() {
        File logFile = new File(Environment.getExternalStorageDirectory() + "/cpwifimonitorlog");
        logFile.delete();
        loadLog();
    }
}
