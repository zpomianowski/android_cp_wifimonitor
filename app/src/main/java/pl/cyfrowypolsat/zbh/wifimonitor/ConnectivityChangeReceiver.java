package pl.cyfrowypolsat.zbh.wifimonitor;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.util.Log;

import java.util.Formatter;
import java.util.Objects;

public class ConnectivityChangeReceiver extends BroadcastReceiver {
    private static final String TAG = "VOWIFI-BR";

    @Override
    public void onReceive(Context context, Intent intent) {
        try {
            Log.i(TAG, "action: " + intent.getAction());
            Log.i(TAG, "component: " + intent.getComponent());
            Bundle extras = intent.getExtras();
            if (extras != null) {
                for (String key : extras.keySet()) {
                    Log.i(TAG, "key [" + key + "]: " +
                            extras.get(key));
                    if (Objects.equals(key, WifiManager.EXTRA_NETWORK_INFO)) {
                        NetworkInfo networkInfo = intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);
                        WifiInfo wifiInfo = intent.getParcelableExtra(WifiManager.EXTRA_WIFI_INFO);
                        Formatter formatter = new Formatter();
                        formatter.format("%s\t%s\t%s\t%s",
                                networkInfo.isConnected(),
                                networkInfo.getTypeName(),
                                networkInfo.getSubtypeName(),
                                networkInfo.getExtraInfo());
                        String text = formatter.toString();
                        if (networkInfo.isConnected()) {
                            formatter = new Formatter();
                            formatter.format("\tRSSI:%d\tF:%d [MHz]\tS:%d [Mb/s]",
                                    wifiInfo.getRssi(),
                                    wifiInfo.getFrequency(),
                                    wifiInfo.getLinkSpeed());
                            text = text + formatter.toString();
                        }

                        Intent service = new Intent(context, SaveIntentService.class);
                        service.setAction(SaveIntentService.ACTION_GETLOG);
                        service.putExtra(SaveIntentService.EXTRA_TEXT, text);
                        context.startService(service);

                        Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                        Ringtone r = RingtoneManager.getRingtone(context, notification);
                        r.play();
                    }
                }
            }
            else {
                Log.i(TAG, "no extras");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
